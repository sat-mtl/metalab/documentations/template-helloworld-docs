versions="`git for-each-ref '--format=%(refname:lstrip=-1)' refs/remotes/origin/ | grep -viE '^(HEAD)$'`"

echo $versions

for current_version in $versions; do
    echo $current_version
	git checkout $current_version
	sphinx-build -b html -D language=en source public/en/$current_version
	sphinx-build -b html -D language=fr source public/fr/$current_version
done
